#include <iostream>
#include <stack>
#include <vector>
#include <string>
#include <set>
#include <map>
#include <queue>
using namespace std;

class Automat {
public:
	Automat(const string& str) : str(str) {
	}
	bool BuildAutomat();
	int solve(char c, int k);
private:
	struct Node;
	int automat_start;
	int _union(int first, int second);
	int _concatinate(int first, int second);
	int _iteration(int first);
	int _add_conversion(char c);
	void _find_terminals(int start);
	void _delete_empty();
	void _dfs_to_empty(int start);
	void _dfs(int start);
	void _build_DKA();
	vector <bool> visited;
	vector<Node> automat;
	const string& str;
	set <int> endings;
	set <int> to_add;
	set <char> alphabet;
	int BFS(int cur);
	void print();
};

struct Automat::Node {
	Node(bool isTerminal) : isTerminal(isTerminal) {}
	map<char, set<int>> go;
	bool isTerminal;
};

bool Automat::BuildAutomat() {
	stack<int> st;
	for (int i = 0; i < str.size(); i++) {
		if ('a' <= str[i] && str[i] <= 'z') {
			st.push(_add_conversion(str[i]));
			alphabet.insert(str[i]);
		}
		else if (str[i] == '+') {
			if (st.size() < 2) {
				return 0;
			}
			int x = st.top();
			st.pop();
			int y = st.top();
			st.pop();
			st.push(_union(x, y));
		}
		else if (str[i] == '.') {
			if (st.size() < 2) {
				return 0;
			}
			int x = st.top();
			st.pop();
			int y = st.top();
			st.pop();
			st.push(_concatinate(y, x));
		}
		else if (str[i] == '*') {
			if (st.size() < 1) {
				return 0;
			}
			int x = st.top();
			st.pop();
			st.push(_iteration(x));
		}
	}
	if (st.size() != 1) {
		return false;
	}
	automat_start = st.top();
	_delete_empty();
	_build_DKA();
	return true;
}

int Automat::_union(int first, int second) {
	Node start(false);
	start.go[' '].insert(first);
	start.go[' '].insert(second);
	automat.push_back(start);
	return automat.size() - 1;
}

int Automat::_concatinate(int first, int second) {
	_find_terminals(first);
	for (auto i : endings) {
		automat[i].go[' '].insert(second);
		automat[i].isTerminal = false;
	}
	return first;
}

int Automat::_iteration(int first) {
	_find_terminals(first);
	int index = automat.size();

	Node start(true); 
	start.go[' '].insert(first);
	automat.push_back(start);
	
	for (auto i : endings) {
		automat[i].go[' '].insert(index);
		automat[i].isTerminal = false;
	}
	return index;
}

int Automat::_add_conversion(char c) {
	Node first(false);
	Node second(true);
	first.go[c].insert(automat.size() + 1);
	automat.push_back(first);
	automat.push_back(second);
	return automat.size() - 2;
}

void Automat::_find_terminals(int start) {
	endings.clear();
	visited = vector<bool>(automat.size(), 0);
	_dfs(start);
}

void Automat::_dfs(int start) {
	if (visited[start]) {
		return;
	}
	visited[start] = true;
	if (automat[start].isTerminal) {
		endings.insert(start);
	}
	for (auto& i : automat[start].go) {
		for (auto j : i.second) {
			_dfs(j);
		}
	}
}

void Automat::_delete_empty() {
	for (int i = 0; i < automat.size(); i++) {
		visited.assign(automat.size(), false);
		to_add.clear();
		_dfs_to_empty(i);
		for (auto j : to_add) {
			for (auto k : automat[j].go) {
				for (auto l : k.second) {
					automat[i].go[k.first].insert(l);
				}
			}
			if (automat[j].isTerminal) {
				automat[i].isTerminal = true;
			}
		}
	}
	for (int i = 0; i < automat.size(); i++) {
		automat[i].go.erase(' ');
	}
}

void Automat::_dfs_to_empty(int start) {
	if (visited[start]) {
		return;
	}
	visited[start] = true;
	to_add.insert(start);
	for (auto i : automat[start].go[' ']) {
		_dfs_to_empty(i);
	}
}

void Automat::_build_DKA() {
//	print();
	map<set<int>, int> result;
	map<pair<int, char>, int> edges;
	vector <bool> isTerminal;
	queue <set<int>> q;
	set<int> cur = { automat_start };
	q.push(cur);
	result[cur] = 0;
	bool term = false;
	set <int> next;
	while (!q.empty()) {
		cur = q.front();
		q.pop();
		term = false;
		for (char i : alphabet) {
			for (int vert : cur) {
				next.clear();
				if (automat[vert].isTerminal) {
					term = true;
				}
				for (int goes : automat[vert].go[i]) {
					next.insert(goes);
				}
				if (!next.empty() && result.count(next) == 0) {
					q.push(next);
					result[next] = result.size();
					edges[make_pair(result[cur], i)] = result.size() - 1;
				}
				else if (!next.empty()) {
					edges[make_pair(result[cur], i)] = result[next];
				}
			}
		}
		isTerminal.push_back(term);
	}
	automat.clear();
	for (int i = 0; i < isTerminal.size(); i++) {
		automat.push_back(Node(isTerminal[i]));
	}
	for (auto j : edges) {
		automat[j.first.first].go[j.first.second].insert(j.second);
	}
	automat_start = 0;
//	print();
	int x = 0;
}

int Automat::solve(char c, int k) {
	int cur = automat_start;
	for (int i = 0; i < k; i++) {
		if (automat[cur].go[c].empty()) {
			return -1;
		}
		for (auto i : automat[cur].go[c]) {
			cur = i;
		}
	}
	int add = BFS(cur); 
	if (add == -1) {
		return add;
	}
	return add + k;
}

int Automat::BFS(int cur) {
	if (automat[cur].isTerminal) {
		return 0;
	}
	queue<int> q;
	q.push(cur);
	vector<int> d(automat.size(), 0);
	visited.assign(d.size(), 0);
	visited[cur] = 1;
	while (!q.empty()){
		int cur = q.front();
		q.pop();
		for (auto i : automat[cur].go) {
			for (int j : i.second) {
				if (visited[j] == 0)
				{
					d[j] = d[cur] + 1;
					if (automat[j].isTerminal) {
						return d[j];
					}
					visited[j] = true;
					q.push(j);
				}
			}
		}
	}
	return -1;
}

void Automat::print() {
	for (int i = 0; i < automat.size(); i++) {
		cout << i << ":" << automat[i].isTerminal << endl;
		for (char c : alphabet) {
			cout << "  " << c;
			for (int j : automat[i].go[c]) {
				cout << " " << j;
			}
			cout << endl;
		}
	}
}

int main() {
	string str;
	char c; 
	int k;
	cin >> str >> c >> k;
	Automat a(str);
	if (!a.BuildAutomat()) {
		cout << "ERROR";
		return 0;
	};
	int answer = a.solve(c, k);
	if (answer == -1) {
		cout << "INF";
	}
	else {
		cout << answer;
	}
	return 0;
}